#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("MS Access Viewer");
    a.setApplicationVersion("1.0.0");
    a.setOrganizationDomain("silvercode.co.za");
    a.setOrganizationName("Ross Simpson");

    MainWindow w;
    w.show();

    return a.exec();
}
