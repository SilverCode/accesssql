#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(QApplication::applicationName() + " v" +
                   QApplication::applicationVersion() + " - " +
                   QApplication::organizationName());

    QSettings settings("config.ini");
    ui->lineEdit->setText(settings.value("database").toString());

    treeview = new QTreeWidget(this);
    treeview->setHeaderLabel("Database");
    root = addRoot("Tables");

    vsplitter = new QSplitter(this);
    vsplitter->setOrientation(Qt::Vertical);
    vsplitter->addWidget(ui->plainTextEdit);
    vsplitter->addWidget(ui->tableView);
    vsplitter->addWidget(ui->errorLog);

    hsplitter = new QSplitter(this);
    hsplitter->addWidget(treeview);
    hsplitter->addWidget(vsplitter);
    ui->verticalLayout->addWidget(hsplitter);

    QList<int> sizes;
    sizes.append(150);
    sizes.append(600);
    hsplitter->setSizes(sizes);

    ui->errorLog->hide();

    QShortcut *shortcut = new QShortcut(QKeySequence("F5"), this);
    connect(shortcut, &QShortcut::activated, this, &MainWindow::on_pushButton_2_clicked);

    if (QFile::exists(ui->lineEdit->text()))
    {
        openDatabase(ui->lineEdit->text());
        ui->plainTextEdit->setFocus();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

QTreeWidgetItem *MainWindow::addRoot(QString name)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(treeview);
    item->setText(0, name);
    treeview->addTopLevelItem(item);

    return item;
}

QTreeWidgetItem *MainWindow::addChild(QTreeWidgetItem *parent, QString name)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);
    item->setText(0, name);
    parent->addChild(item);

    return item;
}

void MainWindow::on_pushButton_clicked()
{
    QFileInfo fileInfo(ui->lineEdit->text());
    QString path = QFile::exists(fileInfo.absolutePath()) ? fileInfo.absolutePath() : QDir::currentPath();
    QString dbFile = QFileDialog::getOpenFileName(this, tr("Open Database"), path, tr("MS Access (*.mdb)"));

    if (!dbFile.isEmpty())
        ui->lineEdit->setText(dbFile);

    openDatabase(dbFile);
}

void MainWindow::on_pushButton_2_clicked()
{
    if (!db.isOpen())
        return;

    model->setQuery(ui->plainTextEdit->toPlainText());

    if (model->lastError().type() != QSqlError::NoError)
    {
        ui->errorLog->setPlainText(model->lastError().text());
        ui->tableView->hide();
        ui->errorLog->show();
        qDebug() << model->lastError();
        return;
    }

    ui->errorLog->hide();
    ui->tableView->show();
}

void MainWindow::openDatabase(QString dbFile)
{
    if (!QFile::exists(dbFile))
    {
        qWarning() << "File" << dbFile << "does not exist";
        return;
    }

    QSettings settings("config.ini");
    settings.setValue("database", dbFile);

    db = QSqlDatabase::addDatabase("QODBC3");
    QString dsn = "DRIVER={Microsoft Access Driver (*.mdb)};FIL={MS Access};DBQ=" + dbFile;
    qDebug() << "Using DSN" << dsn;
    db.setDatabaseName(dsn);
    bool success = db.open();

    if (!success)
    {
        ui->errorLog->setPlainText(db.lastError().text());
        qWarning() << "Failed to open DB";
        return;
    }

    model = new QSqlQueryModel();
    ui->tableView->setModel(model);

    QStringList tables = db.tables();

    foreach (QString table, tables)
    {
        qDebug() << table;
        QTreeWidgetItem *child = addChild(root, table);

        QSqlRecord record = db.record(table);

        for (int i = 0; i < record.count(); ++i)
        {
            QSqlField field = record.field(i);
            qDebug() << field.name();
            QVariant fieldType = field.type();
            QString tmp = field.name() + " (" + fieldType.typeName() + ")";
            addChild(child, tmp);
        }
    }

    treeview->expandItem(root);
}

void MainWindow::on_btnCSV_clicked()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save CSV"), QDir::currentPath(), tr("CSV File (*.csv)"));

    QFile f(filename);

    if (f.open(QFile::WriteOnly | QFile::Truncate | QFile::Text))
    {
        QTextStream data(&f);
        QStringList strList;

        for (int i = 0; i < model->columnCount(); ++i)
        {
            QString output = "\"" + model->headerData(i, Qt::Horizontal).toString() + "\"";
            strList.append(output);
        }

        data << strList.join(",") << "\n";

        for (int i = 0; i < model->rowCount(); ++i)
        {
            strList.clear();
            for (int j = 0; j < model->columnCount(); ++j)
            {
                QModelIndex index = model->index(i, j);
                QVariant value = index.data();
                QString output = value.toString();

                if (value.type() == QVariant::String || value.type() == QVariant::DateTime)
                {
                    output = "\"" + output + "\"";
                }

                strList.append(output);
            }

            data << strList.join(",") << "\n";
        }
    }
}
