#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QSplitter>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QSqlRecord>
#include <QSqlField>
#include <QFileDialog>
#include <QShortcut>
#include <QKeySequence>
#include <QSettings>
#include <QModelIndex>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QTreeWidgetItem* addRoot(QString name);
    QTreeWidgetItem* addChild(QTreeWidgetItem *parent, QString name);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

    void on_btnCSV_clicked();

private:
    Ui::MainWindow *ui;
    QSqlDatabase db;
    QSqlQueryModel *model;
    QSplitter *vsplitter;
    QSplitter *hsplitter;
    QTreeWidget *treeview;
    QTreeWidgetItem *root;

    void openDatabase(QString dbFile);
};

#endif // MAINWINDOW_H
